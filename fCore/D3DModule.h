#ifndef _D3DMODULE_H_
#define _D3DMODULE_H_

// D3DModule.h - Direct3D module

//	LINKER
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include <D3D11.h>
#include <DirectXMath.h>

using namespace DirectX;

class D3DModule
{
public:
	D3DModule();
	D3DModule(const D3DModule&);
	~D3DModule();

	bool Initialize(int, int, bool, HWND, bool, float, float);
	void Shutdown();

	//	BeginScene initializes the buffers so they are blank and ready to be drawn to
	void BeginScene(float, float, float, float);
	//	EndScene tells the swap chain to display 3D scene once all the drawing has completed at the end of each frame
	void EndScene();

	ID3D11Device * GetDevice();
	ID3D11DeviceContext * GetDeviceContext();

	void GetProjectionMatrix(XMMATRIX&);
	void GetOrthoMatrix(XMMATRIX&);
	void GetWorldMatrix(XMMATRIX&);


	void GetVideoCardInfo(char*);

	//	XMMATRIX types must be allocated on heap with 16 byte sizes. Win32 heap allocates in 8 byte sizes. new and delete override required.
	//	Crash proof, high speed.
	void * operator new(unsigned int);
	void operator delete(void*);
private:
	bool m_vsync_enabled;
	char m_videoCardDescription[128];
	IDXGISwapChain * m_swapChain;
	ID3D11Device * m_device;
	ID3D11DeviceContext * m_deviceContext;
	ID3D11RenderTargetView * m_renderTargetView;
	ID3D11Texture2D * m_depthStencilBuffer;
	ID3D11DepthStencilState * m_depthStencilState;
	ID3D11DepthStencilView * m_depthStencilView;
	ID3D11RasterizerState * m_rasterState;

	XMMATRIX m_projectionMatrix;
	XMMATRIX m_orthoMatrix;
	XMMATRIX m_worldMatrix;
};

#endif