#include "texture.h"

Texture::Texture()
{
	m_targetImage = 0;
	m_texture = 0;
	m_textureView = 0;
}

Texture::Texture(const Texture& other)
{

}

Texture::~Texture()
{

}

bool Texture::Initialize(ID3D11Device * device, ID3D11DeviceContext * deviceContext, char * filename)
{
	bool result;
	int height, width;
	D3D11_TEXTURE2D_DESC textureDesc;
	HRESULT hResult;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	unsigned char * dataPtr;
	unsigned int i, j, k, rowStart, columnStart;
	D3D11_SHADER_RESOURCE_VIEW_DESC srDesc;

	//	Load the targa texture data into memory
	result = LoadTarga(filename, height, width);
	if (!result)
		return false;

	//	Setup the desc of the texture
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DYNAMIC;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	textureDesc.MiscFlags = 0;

	//	Create the empty texture
	hResult = device->CreateTexture2D(&textureDesc, NULL, &m_texture);
	if (FAILED(hResult))
		return false;

	//	Lock the texture so it can be written to by the CPU
	hResult = deviceContext->Map(m_texture, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hResult))
		return false;

	dataPtr = (unsigned char*)mappedResource.pData;
	k = (width * height * 4) - (width * 4);

	for (j = 0; j < (unsigned int)height; j++)
	{
		rowStart = j * mappedResource.RowPitch;

		for (i = 0; i < (unsigned int)width; i++)
		{
			columnStart = i * 4;

			dataPtr[rowStart + columnStart + 0] = m_targetImage[k + 2];
			dataPtr[rowStart + columnStart + 1] = m_targetImage[k + 1];
			dataPtr[rowStart + columnStart + 2] = m_targetImage[k + 0];
			dataPtr[rowStart + columnStart + 3] = m_targetImage[k + 3];

			k += 4;
		}
		k -= (width * 8);
	}

	//	Unlock the texture
	deviceContext->Unmap(m_texture, 0);

	delete[] m_targetImage;
	m_targetImage = 0;

	//	Create the shader-resource view
	srDesc.Format = textureDesc.Format;
	srDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srDesc.Texture2D.MostDetailedMip = 0;
	srDesc.Texture2D.MipLevels = 1;

	hResult = device->CreateShaderResourceView(m_texture, &srDesc, &m_textureView);
	if (FAILED(hResult))
		return false;

	return true;
}

void Texture::Shutdown()
{
	//	Release everything
	if (m_textureView)
	{
		m_textureView->Release();
		m_textureView = 0;
	}

	if (m_texture)
	{
		m_texture->Release();
		m_texture = 0;
	}

	if (m_targetImage)
	{
		delete[] m_targetImage;
		m_targetImage = 0;
	}

	return;
}

ID3D11ShaderResourceView * Texture::GetTexture()
{
	return m_textureView;
}

bool Texture::LoadTarga(char * filename, int& height, int& width)
{
	int error, bpp, imageSize;
	FILE * filePtr;
	unsigned int count;
	TargaHeader targaFileHeader;

	error = fopen_s(&filePtr, filename, "rb");
	if (error != 0)
		return false;

	count = fread(&targaFileHeader, sizeof(TargaHeader), 1, filePtr);
	if (count != 1)
		return false;

	height = (int)targaFileHeader.height;
	width = (int)targaFileHeader.width;
	bpp = (int)targaFileHeader.bpp;

	if (bpp != 32)
		return false;

	imageSize = width * height * 4;

	m_targetImage = new unsigned char[imageSize];
	if (!m_targetImage)
		return false;

	count = fread(m_targetImage, 1, imageSize, filePtr);
	if (count != imageSize)
		return false;

	error = fclose(filePtr);
	if (error != 0)
		return false;

	return true;
}